<?php

require '/var/www/gnet_farm/vendor/autoload.php';
// Підключаєм конфіг
require '/var/www/gnet_farm/config.php';

use GnetFarm\GnetFarm\Cloudflare;
use GnetFarm\GnetFarm\Retry;
use GnetFarm\GnetFarm\Aero;
use GnetFarm\GnetFarm\Sidn;
use GnetFarm\GnetFarm\EppExtension;
use Flexihash\Flexihash;
use GnetFarm\GnetFarm\Exceptions\Cloudflare\CloudflareRegisterDomainException;
use GnetFarm\GnetFarm\Exceptions\Cloudflare\CloudflareException;
use GnetFarm\GnetFarm\Exceptions\RequestFailedException;
use GnetFarm\GnetFarm\Registrars\OpenProvider\OpenProvider;


//тут в хеш додаються сервера, на які будуть встановлюватися домени
$hash = new Flexihash();
foreach ($installingHosts as $installingHost) {
    $hash->addTarget($installingHost['ip'], $installingHost['weight']);
}


echo date('Y-m-d H:i:s').' - Started *****************'.PHP_EOL;

//дістаєм індекс першого домена, який буде куплений
$indexOfFirstDomainToBuy = file_get_contents('/var/www/gnet_farm/files/indexOfFirstToBuy.txt');
//дістаємо домени, які будуть куплені
$domains = file('/var/www/gnet_farm/files/toRegisterSidn.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$domains = array_slice($domains, $indexOfFirstDomainToBuy);

if (empty($domains)) {
    echo 'No domains to setup'.PHP_EOL;
    exit();
}

//файл в який будуть зберігаютись куплені домени
$boughtFile = '/var/www/gnet_farm/files/boughtByDay/'.date('m-d').'.txt';

//якщо файл існує, то дістаєм домени з нього, та рахуєм скільки доменів залишилось купити
if (file_exists($boughtFile)) {
    $boughtDomains = file($boughtFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
} else {
    $boughtDomains = [];
}

$amountToBuy = $domainsPerTime - count($boughtDomains);
echo "Amount to buy: $amountToBuy".PHP_EOL;

$sidn = new Sidn();

//в цьому циклі відбувається купівля доменів
$count = 0;
$bought = 0;
while ($bought < $amountToBuy) {

    try {
        $sidn->registerDomain($domains[$count]);
    }
    catch (Exception $e) {
        echo "Domain {$domains[$count]} was not bought".PHP_EOL;
        echo $e->getMessage().PHP_EOL;
        echo "Recreating DRS connection".PHP_EOL;
        $sidn = new Sidn();
        sleep(5);
        file_put_contents('/var/www/gnet_farm/files/indexOfFirstToBuy.txt', $indexOfFirstDomainToBuy + ++$count);
        continue;
    }

    file_put_contents($boughtFile, $domains[$count].PHP_EOL, FILE_APPEND);
    echo "Domain {$domains[$count]} was bought".PHP_EOL;
    $boughtDomains[] = $domains[$count];
    $bought++;
    //тут ставиться затримка між покупкою доменів
    sleep(mt_rand(15, 30));
    file_put_contents('/var/www/gnet_farm/files/indexOfFirstToBuy.txt', $indexOfFirstDomainToBuy + ++$count);
}

$boughtDomains = file($boughtFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

$eppExtension = new EppExtension();
foreach ($boughtDomains as $domain) {
    try {
        $eppExtension->setAutoDelete($domain);
    } catch (Exception $e) {
        echo $e->getMessage().PHP_EOL;
        sleep(5 * 60);
        continue;
    }
}
unset($eppExtension);

$failedDomains = [];
$activeCloudflareAccounts = $clfAccounts;
$sidn = new Sidn();
//в цьому циклі відбувається встановлення доменів на сервери
foreach ($boughtDomains as $num => $domain) {

    $accIndex = array_rand($activeCloudflareAccounts);
    $cfCreds = $activeCloudflareAccounts[$accIndex];
    echo '--------------------------'.PHP_EOL;
    echo "Domain $domain is being setup".PHP_EOL;
    echo "Using account ".$cfCreds['apiEmail'].PHP_EOL;

    $cld = new Cloudflare($domain, $cfCreds);
    //обгортаєм клаудфлеїр в клас Retry, який буде пробувати 3(4) рази відправляти запит перед тим як кинути exception
    $cldTryHarder = new Retry($cld, 3);

    try {
        $cldTryHarder->registerDomain();
        echo "Domain $domain added to cloudflare".PHP_EOL;
    } catch (CloudflareException $e) {

        //тут я намагався обробити помилки, в залежності від коду, проте не факт що працює це вірно
        // наприклад є помилка коли в домена погана репутація, і тоді домен потрібно скіпнути
        //а є помилка коли вийшов ліміт на реєстрацію доменів в годину, і тоді потрібно домен перезапустити але пізніше
        //нажаль поки не було кейсів і не відомо чи відпрацює правильно

        //тут обробляються помилки звязані з лімітом на реєстрацію доменів
        //акаунт видаляється з пула активних. Якщо не залишилось активних, то скрипт спить годину і пул оновлюється
        if (in_array($e->getCode(), [1117, 1118])) {
            unset($activeCloudflareAccounts[$accIndex]);
            echo "Cloudflare ".$e->getCode()." error".$e->getMessage().PHP_EOL;
            echo "Account ".$cfCreds['apiEmail']." removed".PHP_EOL;

            if (empty($activeCloudflareAccounts)) {
                echo "All accounts are dead. Sleeping for 1 hour".PHP_EOL;
                sleep(60 * 60);
                $sidn = new Sidn();
                $activeCloudflareAccounts = $clfAccounts;
            }
            // тут якщо кейс коли домен потрібно перевстановити, то він додається в кінець списку, але таких кейсів теж
            // ще не було і не факт що правильно відпрацює
            $boughtDomains[] = $domain;
            unset($boughtDomains[$num]);
        }
        else {
            echo "Cloudflare ".$e->getCode()." error: ".$e->getMessage().PHP_EOL;
            $failedDomains[] = $domain;
            continue;
        }
    } catch (RequestFailedException $e) {
        // тут обробляється помилка проксі, так як проксі привязаний до аккаунта клодфлару, то якщо він не працює, то
        //видаляється з пула активних акаунтів, якщо акаунтів не залишилось то скрипт спить 1 годину, і пул оновлюється
        echo "Request failed error: ".$e->getMessage().PHP_EOL;

        unset($activeCloudflareAccounts[$accIndex]);
        echo "Account ".$cfCreds['apiEmail']." removed".PHP_EOL;

        if (empty($activeCloudflareAccounts)) {
            echo "All accounts are dead. Sleeping for 1 hour".PHP_EOL;
            sleep(60 * 60);
            $sidn = new Sidn();
            $activeCloudflareAccounts = $clfAccounts;
        }

        $boughtDomains[] = $domain;
        unset($boughtDomains[$num]);
        continue;
    }

    $server = $hash->lookup($domain);
    echo "Server: $server".PHP_EOL;

    try {
        $cldTryHarder->enableHttps();
        echo "Https enabled".PHP_EOL;
        $cldTryHarder->enableCacheEverything();
        echo "Cache everything enabled".PHP_EOL;
        $cldTryHarder->addDnsRecord($domain, $server);
        echo "DNS record $domain added".PHP_EOL;
        $cldTryHarder->addDnsRecord('*', $server);
        echo "DNS record * added".PHP_EOL;
        $nameservers = $cldTryHarder->getNs($domain);
        echo "Nameservers: ".implode(', ', $nameservers).PHP_EOL;
    } catch (Exception $e) {
        $failedDomains[] = $domain;
        echo "Domain $domain failed".PHP_EOL;
        continue;
    }

    //тут ми ставимо мітку, на основні серваки, для групуванню доменів по даті встановлення
    foreach ($aeroMainHosts as $aeroMainHost) {
        $aerospike = new Aerospike([
            'hosts' => [
                ['addr' => $aeroMainHost, 'port' => 3091],
            ]]);
        $aero = new Aero($aerospike, 'gnet');
        $aero->put('gnetway', $domain, ['mark' => date('m-d')]);
    }

    //тут міняємо неймсервера на клаудфлейровські
    try {
        $sidn->changeNs($domain, $nameservers);
    } catch (Exception $e) {
        echo "Registrar exception: ".$e->getMessage().PHP_EOL;
        $failedDomains[] = $domain;
        continue;
    } catch (Error $e) {
        echo "Registrar error: ".$e->getMessage().PHP_EOL;
        $failedDomains[] = $domain;
        continue;
    }
}
unset($sidn);

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, true);
foreach ($boughtDomains as $boughtDomain) {
    //check that domain responds with 404 error code
    curl_setopt($ch, CURLOPT_URL, 'https://'.$boughtDomain);
    $res = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($code != 404) {
        echo "Domain $boughtDomain responds with $code code".PHP_EOL;
        $failedDomains[] = $boughtDomain;
    }
}

$finalDomains = array_diff($boughtDomains, $failedDomains);
file_put_contents('/var/www/gnet_farm/files/'.date('m-d').'Fails.txt', implode(PHP_EOL, $failedDomains));

//спимо годинку, і встановлюєм редіректи
sleep(60 * 60);
foreach ($aeroPoolHosts as $aeroPoolHost) {
    $aerospike = new Aerospike([
        'hosts' => [
            ['addr' => $aeroPoolHost, 'port' => 3091],
        ]]);
    $aero = new Aero($aerospike, 'gnet');

    $domainChunks = json_decode($aero->get('gnet-pool-302-redirect', 'bulk')['data'], true);
    $domainChunks = array_filter($domainChunks, function ($item) {
        return $item['expires'] > time();
    });

    $domainsForRedirect = array_merge(...array_column($domainChunks, 'domains'));

    if (count(array_intersect($domainsForRedirect, $domains)) > 0) {
        echo "Domain already exists in pool\n";
        exit();
    }

    $domainChunks[] = [
        'domains' => $finalDomains,
        'expires' => time() + 20 * 60 * 60
    ];

    $aero->put('gnet-pool-302-redirect', 'bulk', ['data' => json_encode($domainChunks)]);
}

foreach ($aeroMainHosts as $aeroMainHost) {
    $aerospike = new Aerospike([
        'hosts' => [
            ['addr' => $aeroMainHost, 'port' => 3091],
        ]]);
    $aero = new Aero($aerospike, 'gnet');

    $domainChunks = json_decode($aero->get('gnet-way-302-redirect', 'bulk')['data'], true);
    $domainChunks = array_filter($domainChunks, function ($item) {
        return $item['expires'] > time();
    });

    $domainsForRedirect = array_merge(...array_column($domainChunks, 'domains'));

    if (count(array_intersect($domainsForRedirect, $domains)) > 0) {
        echo "Domain already exists in way\n";
        exit();
    }

    $randomDomains = function() use ($finalDomains) {
        $randomCount = (int)(count($finalDomains) * 0.5);
        $randomKeys = array_rand($finalDomains, $randomCount);
        if (!is_array($randomKeys)) {
            $randomKeys = [$randomKeys];
        }
        $randomDomains = array();
        foreach ($randomKeys as $key) {
            $randomDomains[] = $finalDomains[$key];
        }

        return $randomDomains;
    };

    $domainChunks[] = [
        'domains' => $randomDomains,
    ];

    $aero->put('gnet-way-302-redirect', 'bulk', ['data' => json_encode($domainChunks)]);
}

echo "Done".PHP_EOL;
